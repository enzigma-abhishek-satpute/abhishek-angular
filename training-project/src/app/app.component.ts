import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app root module with its color of own(h1 tag)';
  checkngif=true;
  switchvalue=4;
  btnclick(){
    alert('alert is clicked and example of component directive as it changes the dom apperance or bheviour');
  }

}
